var trackerElements = document.getElementsByClassName( "tracker" );

for ( var i = 0; i < trackerElements.length; i++ ) {
    trackerElements[i].textContent = trackerElements[i].textContent.replace( /^Theme: /, "" );
}

[ "tracker", "status", "fixed_version", "author", "assigned_to" ].forEach( function( className ) {

    var elements = document.getElementsByClassName( className );

    for ( var i = 0; i < elements.length; i++ ) {
        elements[i].title = elements[i].textContent;
    }

} );
